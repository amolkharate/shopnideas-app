import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeliverydetailsPageRoutingModule } from './deliverydetails-routing.module';

import { DeliverydetailsPage } from './deliverydetails.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeliverydetailsPageRoutingModule
  ],
  declarations: [DeliverydetailsPage]
})
export class DeliverydetailsPageModule {}
