import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeliverydetailsPage } from './deliverydetails.page';

const routes: Routes = [
  {
    path: '',
    component: DeliverydetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeliverydetailsPageRoutingModule {}
