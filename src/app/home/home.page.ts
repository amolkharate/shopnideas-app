  import { Component,OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { environment, SERVER_URL } from '../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

	slideOptsOne = {
                 initialSlide: 0,
                 slidesPerView: 1,
                 autoplay:true 
                 };
  bannerdata; 
  homepagecakeHomeproduct;
  homepageflowersHomeproduct;
  homepagecomboHomeproduct;
  constructor(private route: ActivatedRoute, public http: HttpClient){}


   ngOnInit() {
		this.getHomePageData();
    console.log(SERVER_URL);
   }

   getHomePageData(){
        this.http.get(SERVER_URL+'Api/Home/getHomePageData').subscribe(WORKFLOW_DATA=>{
          		this.bannerdata  = WORKFLOW_DATA['data']['banner'];
              this.homepagecakeHomeproduct = WORKFLOW_DATA['data']['cakeHome'];
              this.homepageflowersHomeproduct = WORKFLOW_DATA['data']['flowereHome'];
              this.homepagecomboHomeproduct = WORKFLOW_DATA['data']['comboHome'];
    		})
    }

    

}
  