import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment, SERVER_URL } from '../../environments/environment';

@Component({
  selector: 'app-productdetails', 
  templateUrl: './productdetails.page.html',
  styleUrls: ['./productdetails.page.scss'],
})
export class ProductdetailsPage implements OnInit {

  product_id: string;
  productdata:string;
  weightdata;
  productname:string;
  price:string;
  weight:string;
  product_image:string;
  product_description:string;
  constructor(private route: ActivatedRoute, public http:HttpClient) {}

  ngOnInit() {
  	this.route.queryParams.subscribe(params => {
        this.product_id = params['product_id'];
    });
    this.getProducts();
  }

  getProducts(){
  	this.http.post<any>(SERVER_URL+'Api/Home/getProductDetails',{product_id:this.product_id }).subscribe(WORKFLOW_DATA=>{
         this.productdata  = WORKFLOW_DATA['data'];
         this.weightdata = WORKFLOW_DATA['weight'];
         this.productname = this.productdata['product_name'];
         this.price = this.productdata['price'];
         this.weight = this.productdata['weight_title'];
         this.product_image = this.productdata['product_image'];
         this.product_description =  this.productdata['product_description'];
    })
  }

}
 