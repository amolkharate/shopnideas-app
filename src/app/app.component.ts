import { Component,OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HttpClient } from '@angular/common/http';
import { environment, SERVER_URL } from '../environments/environment';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
}) 
export class AppComponent  implements OnInit {
  public pages = [
    {
      title: 'Main',
      url: '/home',
      icon: 'list' 
    },
    {
      title: 'Cool framwork',
      children:[
          {
            title: 'IONIC',
            url: '/home',
            icon: 'list' 
          },
          {
            title: 'FLUTOR',
            url: '/home',
            icon: 'list' 
          }
      ]
    },
    {
      title: 'Flowers',
      children:[
          {
            title: 'Roese',
            url: '/home',
            icon: 'list' 
          },
          {
            title: 'India',
            url: '/home',
            icon: 'list' 
          }
      ]
    }
  ];

 
  menu;
  menu_title;
   submenu;
   category_id;
   router;
   showForce;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
     public http: HttpClient,
     private route: ActivatedRoute
  ) {

    this.initializeApp();
  }

  ngOnInit() {
    this.getMenu();
  }

  getMenu(){
        this.http.get(SERVER_URL+'Api/Home/getmenu').subscribe(WORKFLOW_DATA=>{
              this.menu  = WORKFLOW_DATA['data'];
              console.log( this.menu );
        })
    }

    menuItemForce(): void {
    this.showForce = !this.showForce;
  } 

    // open(id: number) {
    //       this.router.navigateByUrl(this.router.url + '/' + id);
    // }

  initializeApp() {

    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
