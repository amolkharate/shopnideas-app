import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment, SERVER_URL } from '../../environments/environment';


@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {
 
  category_id: string;
  productdata;
  category_name;
 constructor(private route: ActivatedRoute, public http:HttpClient) {}

  ngOnInit() {
  	this.route.queryParams.subscribe(params => {
        this.category_id = params['category_id'];
        this.getProducts();
    });
  }

  getProducts(){
  	this.http.post<any>(SERVER_URL+'Api/Home/getProductCategoryWise',{category_id:this.category_id }).subscribe(WORKFLOW_DATA=>{
         this.productdata  = WORKFLOW_DATA['data'];
         this.category_name = WORKFLOW_DATA['category_name'];
         console.log(this.category_name);
    })
  }

}
