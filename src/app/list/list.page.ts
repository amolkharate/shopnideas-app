import { Component, OnInit } from '@angular/core';
import { environment, SERVER_URL } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
}) 
export class ListPage implements OnInit {
  private selectedItem: any;

   menu_title;
   submenu;
   category_id;
   constructor(private route: ActivatedRoute, public http:HttpClient) {
     console.log('aaa');

   }
  // private icons = [
  //   'flask',
  //   'wifi',
  //   'beer',
  //   'football',
  //   'basketball',
  //   'paper-plane',
  //   'american-football',
  //   'boat',
  //   'bluetooth',
  //   'build'
  // ];
  // public items: Array<{ title: string; note: string; icon: string }> = [];
  // constructor() {
  //   for (let i = 1; i < 11; i++) {
  //     this.items.push({
  //       title: 'Item ' + i,
  //       note: 'This is item #' + i,
  //       icon: this.icons[Math.floor(Math.random() * this.icons.length)]
  //     });
  //   }
  // } 

  ngOnInit() { 

    this.route.queryParams.subscribe(params => {
        this.category_id = params['category_id'];
    });
    alert(this.category_id);
    this.getSubMenu();
  }

   getSubMenu(){
    this.http.post<any>(SERVER_URL+'Api/Home/getsubmenu',{category_id:this.category_id }).subscribe(WORKFLOW_DATA=>{
         this.menu_title  = WORKFLOW_DATA['data']['menu_title'];
         this.submenu  = WORKFLOW_DATA['data'];
         console.log(this.submenu);
    })
  }

  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}
